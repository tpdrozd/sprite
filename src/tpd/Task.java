package tpd;

public class Task extends AbstractTask {
	
	private String	label;
	
	public Task ( String label ) {
		this.label = label;
	}
	
	public void done () {
		println("Task '" + label + "' done.");
	}
}
