package tpd.about;

public class About {
	
	private static void println ( String s ) {
		System.out.println(s);
	}
	
	public static void main () {
		println("About Us");
		
		MeTask meTask = new MeTask();
		meTask.doMe();
		
		MaryTask maryTask = new MaryTask();
		maryTask.doMary();
	}
	
}
