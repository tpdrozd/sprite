package tpd;

import java.io.IOException;
import java.util.Locale;

public abstract class AbstractTask {
	
	protected void print ( String s ) {
		System.out.print(s);
	}
	
	protected void println ( String s ) {
		System.out.println(s);
	}
	
	protected void printlnColor ( String s ) {
		System.out.println("\t\t\t\t.. " + s + " ..");
	}
	
	protected void printf ( String format, Object... args ) {
		System.out.printf(format, args);
	}
	
	protected void printf ( Locale locale, String format, Object... args ) {
		System.out.printf(locale, format, args);
	}
	
	protected int read () {
		try {
			int read = System.in.read();
			return read;
		}
		catch ( IOException e ) {
			return 0;
		}
	}
}
