package tpd;


public class NewsTask extends AbstractTask {

	private String news;
	
	public NewsTask ( String news ) {
		this.news = news;
	}

	public void doNews () {
		println("The news is: " + news);
	}
	
}
